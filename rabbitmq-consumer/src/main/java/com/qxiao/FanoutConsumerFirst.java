package com.qxiao;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class FanoutConsumerFirst {
    public static void main(String[] args) throws IOException, TimeoutException {
// 1. 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();

        // 2. 设置参数
        factory.setHost("192.168.13.130");
//        factory.setPort(5672);
        factory.setVirtualHost("/msg");
        factory.setUsername("guest");
        factory.setPassword("guest");
        // 3. 创建连接 Connection
        Connection connection = factory.newConnection();
        // 4. 创建channel
        Channel channel = connection.createChannel();

        // 5. 消费消息
        /**
         * String queue, 队列名称
         * boolean autoAck, 自动确认
         * Consumer callback 回调对象
         */
        DefaultConsumer consumer = new DefaultConsumer(channel){
            /**
             * 回调方法，当接收到消息后，会自动执行该方法
             * @param consumerTag 消息的标识
             * @param envelope 获取一些信息，交换机，routingkey
             * @param properties 配置信息
             * @param body 数据
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("打印控制台： " + new String(body));
            }
        };
        channel.basicConsume("fanout_queue_first", true, consumer);
    }
}
