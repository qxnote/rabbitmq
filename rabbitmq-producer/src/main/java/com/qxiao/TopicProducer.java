package com.qxiao;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class TopicProducer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1. 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();

        // 2. 设置参数
        factory.setHost("192.168.13.130");
        factory.setPort(5672);
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setVirtualHost("/msg");

        // 3. 创建connection
        Connection connection = factory.newConnection();

        // 4. 创建channel
        Channel channel = connection.createChannel();

        // 5. 创建交换机
        /**
         * String exchange, 交换机名称
         * BuiltinExchangeType type, 交换机类型
         *     DIRECT("direct"),    定向
         *     FANOUT("fanout"),    广播
         *     TOPIC("topic"),      通配符
         *     HEADERS("headers");  参数匹配
         * boolean durable, 是否持久化
         * boolean autoDelete, 是否自动删除
         * boolean internal, 内部使用，一般为false
         * Map<String, Object> arguments 参数
         */
        String exchangeName = "topic_exchange";
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, true, false, false, null);

        // 6. 创建队列queue
        /**
         * String queue, 队列名称
         * boolean durable, 是否持久化
         * boolean exclusive, 是否独占，只能有一个消费者监听这个队列; 当connection关闭时，是否删除队列
         * boolean autoDelete, 是否自动删除, 当没有consumer时，自动删除
         * Map<String, Object> arguments
         */
        String topicQueueFirst = "topic_queue_first";
        String topicQueueSecond = "topic_queue_second";
        channel.queueDeclare(topicQueueFirst, true, false, false, null);
        channel.queueDeclare(topicQueueSecond, true, false, false, null);

        // 7. 绑定交换机和队列
        /**
         * String queue, 队列名称
         * String exchange, 交换机名称
         * String routingKey, 路由
         * Map<String, Object> arguments 参数
         */
        // routingKey 系统日志error和订单order存数据库
        channel.queueBind(topicQueueFirst, exchangeName, "#.error", null);
        channel.queueBind(topicQueueFirst, exchangeName, "order.*", null);

        // routingKey 所有消息打印控制台
        channel.queueBind(topicQueueSecond, exchangeName, "*.*", null);

        // 8. 发送消息
        /**
         * String exchange, 交换机名称
         * String routingKey, 路由名称
         * AMQP.BasicProperties props, 配置信息
         * byte[] body 发送的消息数据
         */
        channel.basicPublish(exchangeName, "order.info", null, "order info".getBytes());
        channel.basicPublish(exchangeName, "order.error", null, "error info".getBytes());
        channel.basicPublish(exchangeName, "log.info", null, "log info".getBytes());

        // 9. 释放资源
        channel.close();
        connection.close();
    }


}
