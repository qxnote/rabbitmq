package com.qxiao;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SimpleProducer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 1. 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();

        // 2. 设置参数
        factory.setHost("192.168.13.130");
//        factory.setPort(5672);
        factory.setVirtualHost("/msg");
        factory.setUsername("guest");
        factory.setPassword("guest");
        // 3. 创建连接 Connection
        Connection connection = factory.newConnection();
        // 4. 创建channel
        Channel channel = connection.createChannel();
        // 5. 创建队列 Queue
        /**
         * String queue, 队列名称
         * boolean durable, 是否持久化
         * boolean exclusive, 是否独占，只能有一个消费者监听这个队列; 当connection关闭时，是否删除队列
         * boolean autoDelete, 是否自动删除, 当没有consumer时，自动删除
         * Map<String, Object> arguments
         */
        channel.queueDeclare("simple_queue", true, false, false, null);
        // 6. 发送消息
        /**
         * String exchange, 交换机名称
         * String routingKey, 路由名称
         * AMQP.BasicProperties props, 配置信息
         * byte[] body 发送的消息数据
         */
        channel.basicPublish("", "simple_queue", null, "Hello!".getBytes());

        // 7. 释放资源
        channel.close();
        connection.close();

    }
}