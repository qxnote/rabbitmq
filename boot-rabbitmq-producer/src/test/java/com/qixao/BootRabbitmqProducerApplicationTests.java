package com.qixao;

import com.qixao.config.RabbitmqConfig;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BootRabbitmqProducerApplicationTests {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void testBootProducer() {
        rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_NAME, "boot.log", "hello boot topic");

    }

}
