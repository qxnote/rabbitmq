package com.qixao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootRabbitmqProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootRabbitmqProducerApplication.class, args);
    }

}
