package com.qixao.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitmqListener {
    @RabbitListener(queues = "boot_queue")
    public void listenQueue(Message message) {
        System.out.println(message);
        System.out.println(new String(message.getBody()));
    }
}
