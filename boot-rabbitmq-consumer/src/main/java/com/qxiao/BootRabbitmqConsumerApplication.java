package com.qxiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootRabbitmqConsumerApplication {
    public static void main(String[] args) {

        SpringApplication.run(BootRabbitmqConsumerApplication.class);
    }
}