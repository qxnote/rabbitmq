package com.qxiao;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration("classpath*:spring-rabbitmq.xml")
public class Main {
    @Autowired
    private RabbitTemplate rabbitTemplate;

  /*  public static void main(String[] args) {
        rabbitTemplate.convertAndSend();

    }*/
}