import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-rabbitmq.xml")
public class ProducerTest {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        rabbitTemplate.convertAndSend("spring_queue", "Hello spring rabbitmq");
    }

    @Test
    public void testDirect() {

        rabbitTemplate.convertAndSend("spring_direct_exchange", "order", "Hello spring direct rabbitmq");
    }

    @Test
    public void testTopic() {

        rabbitTemplate.convertAndSend("spring_topic_exchange", "order.haha", "Hello spring topic rabbitmq");
    }
}
