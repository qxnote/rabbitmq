package com.qxiao.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class DirectQueueListener implements MessageListener {
    @Override
    public void onMessage(Message message) {

        System.out.println("direct message" + new String(message.getBody()));
    }
}
