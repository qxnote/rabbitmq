package com.qxiao.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class TopicQueueListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        System.out.println("topic message: " + new String(message.getBody()));
    }
}
